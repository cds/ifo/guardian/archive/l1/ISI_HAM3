from guardian import GuardState

from . import util
from . import const as wd_const
from ..isolation import util as iso_util
from ..isolation import const as iso_const
from .. import const as top_const
from ..damping import util as damp_util


class WATCHDOG_TRIPPED_DEISOLATING(GuardState):
    request = False
    index = 11

    def main(self):
        iso_util.clear_iso_filters()
        if (iso_const.ISOLATION_CONSTANTS['SWITCH_GS13_GAIN']) & (top_const.CHAMBER_TYPE != 'BSC_ST1')  & (top_const.CHAMBER_TYPE != 'HPI'):
            iso_util.switch_gs13_gain('LOW',iso_const.ISOLATION_CONSTANTS['GS13_GAIN_DOF']) 
        if (top_const.CHAMBER_TYPE != 'HPI'):
            iso_util.turn_FF('OFF', iso_const.ISOLATION_CONSTANTS['FF_DOF'])

    def run(self):
        notify('WATCHDOG TRIP: DEISOLATING (2)')

        watchdog_state = util.read_watchdog_state()
        if watchdog_state != wd_const.WATCHDOG_DEISOLATING_STATE:
            if watchdog_state == wd_const.WATCHDOG_ARMED_STATE:
                return 'INIT'
            if watchdog_state == wd_const.WATCHDOG_DAMPING_STATE:
                return 'WATCHDOG_TRIPPED_DAMPING'
            if watchdog_state == wd_const.WATCHDOG_FULL_SHUTDOWN_STATE:
                return 'WATCHDOG_TRIPPED_FULL_SHUTDOWN'
        return False


class WATCHDOG_TRIPPED_DAMPING(GuardState):
    request = False
    index = 12

    def main(self):
        iso_util.clear_iso_filters()
        if (iso_const.ISOLATION_CONSTANTS['SWITCH_GS13_GAIN']) & (top_const.CHAMBER_TYPE != 'BSC_ST1')  & (top_const.CHAMBER_TYPE != 'HPI'):
            iso_util.switch_gs13_gain('LOW',iso_const.ISOLATION_CONSTANTS['GS13_GAIN_DOF']) 
        if (top_const.CHAMBER_TYPE != 'HPI'):
            iso_util.turn_FF('OFF', iso_const.ISOLATION_CONSTANTS['FF_DOF'])
    
    def run(self):
        notify('WATCHDOG TRIP: DAMPING_ONLY (3)')

        watchdog_state = util.read_watchdog_state()
        if watchdog_state != wd_const.WATCHDOG_DAMPING_STATE:
            if watchdog_state == wd_const.WATCHDOG_ARMED_STATE:
                return 'INIT'
            if watchdog_state == wd_const.WATCHDOG_DEISOLATING_STATE:
                return 'WATCHDOG_TRIPPED_DEISOLATING'
            if watchdog_state == wd_const.WATCHDOG_FULL_SHUTDOWN_STATE:
                return 'WATCHDOG_TRIPPED_FULL_SHUTDOWN'
        return False


class WATCHDOG_TRIPPED_FULL_SHUTDOWN(GuardState):
    request = False
    index = 13

    def main(self):
        iso_util.clear_iso_filters()
        if top_const.CHAMBER_TYPE == 'HPI':
            return True
        else:
            damp_util.clear_damp_filters()
        if (iso_const.ISOLATION_CONSTANTS['SWITCH_GS13_GAIN']) & (top_const.CHAMBER_TYPE != 'BSC_ST1')  & (top_const.CHAMBER_TYPE != 'HPI'):
            iso_util.switch_gs13_gain('LOW',iso_const.ISOLATION_CONSTANTS['GS13_GAIN_DOF']) 
        if (top_const.CHAMBER_TYPE != 'HPI'):
            iso_util.turn_FF('OFF', iso_const.ISOLATION_CONSTANTS['FF_DOF'])

    def run(self):
        notify('WATCHDOG TRIP: FULL_SHUTDOWN (4)')

        watchdog_state = util.read_watchdog_state()
        if watchdog_state != wd_const.WATCHDOG_FULL_SHUTDOWN_STATE:
            if watchdog_state == wd_const.WATCHDOG_ARMED_STATE:
                return 'INIT'
            if watchdog_state == wd_const.WATCHDOG_DEISOLATING_STATE:
                return 'WATCHDOG_TRIPPED_DEISOLATING'
            if watchdog_state == wd_const.WATCHDOG_DAMPING_STATE:
                return 'WATCHDOG_TRIPPED_DAMPING'
        return False


class WATCHDOG_TRIPPED_DACKILL(GuardState):
    request = False
    index = 14

    def main(self):
        iso_util.clear_iso_filters()
#        damp_util.clear_damp_filters()
        if (iso_const.ISOLATION_CONSTANTS['SWITCH_GS13_GAIN']) & (top_const.CHAMBER_TYPE != 'BSC_ST1')  & (top_const.CHAMBER_TYPE != 'HPI'):
            iso_util.switch_gs13_gain('LOW',iso_const.ISOLATION_CONSTANTS['GS13_GAIN_DOF']) 
        if (top_const.CHAMBER_TYPE != 'HPI'):
            iso_util.turn_FF('OFF', iso_const.ISOLATION_CONSTANTS['FF_DOF'])

    def run(self):
        notify('WATCHDOG TRIP: DACKILL')

        if wd_util.is_dackill_tripped():
            return False

        watchdog_state = util.read_watchdog_state()
        if watchdog_state != wd_const.WATCHDOG_FULL_SHUTDOWN_STATE:
            if watchdog_state == wd_const.WATCHDOG_ARMED_STATE:
                return 'INIT'
            if watchdog_state == wd_const.WATCHDOG_DEISOLATING_STATE:
                return 'WATCHDOG_TRIPPED_DEISOLATING'
            if watchdog_state == wd_const.WATCHDOG_DAMPING_STATE:
                return 'WATCHDOG_TRIPPED_DAMPING'
            if watchdog_state == wd_const.WATCHDOG_FULL_SHUTDOWN_STATE:
                return 'WATCHDOG_TRIPPED_FULL_SHUTDOWN'
        return False
